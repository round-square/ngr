import { AbstractControl } from 'ngx-strongly-typed-forms'
import { Observable } from 'rxjs'
import { startWith } from 'rxjs/operators'

export function valueChange<T>(c: AbstractControl<T>): Observable<T> {
  return c.valueChanges.pipe(startWith(c.value))
}
