import { getDate, getMonth, getYear } from 'date-fns'

/**
 * Interface of the model of the NgbDatepicker and NgbInputDatepicker directives
 */
export interface DateStruct {
  /**
   * The year, for example 2016
   */
  year: number
  /**
   * The month, for example 1=Jan ... 12=Dec
   */
  month: number
  /**
   * The day of month, starting at 1
   */
  day: number
}

export const DateStruct = {
  toDate: (date: DateStruct): Date =>
    new Date(date.year, date.month - 1, date.day),
  fromDate: (date: Date): DateStruct => ({
    year: getYear(date),
    month: getMonth(date) + 1,
    day: getDate(date),
  }),
}
