export interface Dictionary<T> {
  [i: string]: T
}
