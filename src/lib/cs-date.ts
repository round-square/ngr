import { NgModule, Pipe, PipeTransform } from '@angular/core'

export type CsDate = number

// ticks are recorded from 1/1/1; get microtime difference from 1/1/1/ to 1/1/1970
const epochMicrotimeDiff = Math.abs(new Date(0, 0, 1).setFullYear(1))

export const CsDate = {
  parse: (date: Date): CsDate => {
    const value = date.getTime()
    const ticksToMicrotime = value + epochMicrotimeDiff
    return ticksToMicrotime * 10000
  },
  toDate: (value: CsDate) => {
    // ticks are in nanotime; convert to microtime
    const ticksToMicrotime = value / 10000

    // new date is ticks, converted to microtime, minus difference from epoch microtime
    return new Date(ticksToMicrotime - epochMicrotimeDiff)
  },
}

@Pipe({
  name: 'csDate',
})
export class CsDatePipe implements PipeTransform {
  transform(value: CsDate): Date {
    return CsDate.toDate(value)
  }
}

@NgModule({
  exports: [CsDatePipe],
  declarations: [CsDatePipe],
})
export class CsDatePipeModule {}
