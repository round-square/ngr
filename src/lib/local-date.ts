import { format, parse } from 'date-fns'

export type LocalDate = string

export const localDateFormat = 'dd/MM/yyyy'

export const LocalDate = {
  toDate: (value: LocalDate): Date | null => {
    const date = parse(value, localDateFormat, new Date())
    return isNaN(date.getTime()) ? null : date
  },
  parse: (value: Date): LocalDate => {
    return format(value, localDateFormat)
  },
}
