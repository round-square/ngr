import { ofType } from '@ngrx/effects'
import { Observable } from 'rxjs'
import { filter, first } from 'rxjs/operators'

export abstract class Action {
  public readonly type: any

  constructor() {
    this.type = this.type
  }
}

export interface ActionClass<T extends Action> {
  prototype: T

  new (...args: any[]): T
}

export function ActionType(type: string) {
  return function<T extends Action>(actionClass: ActionClass<T>) {
    Object.assign(actionClass.prototype, { type })
  }
}

export function isAction<T extends Action>(
  action: Action,
  actionClass: ActionClass<T>,
): action is T {
  return action.type === actionClass.prototype.type
}

export function ofAction<T extends Action>(...classes: ActionClass<T>[]) {
  return (source: Observable<Action>): Observable<T> =>
    <any>ofType(...classes.map(c => c.prototype.type))(<any>source)
}

export abstract class MatchAction extends Action {
  abstract matchId: number
}

export function matchAction<T extends MatchAction>(
  ctor: ActionClass<T>,
  matchId: number,
) {
  return (source: Observable<Action>): Observable<T> =>
    source.pipe(
      ofAction(ctor),
      filter(a => a.matchId === matchId),
      first(),
    )
}
