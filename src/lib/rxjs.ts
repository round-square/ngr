import { NgZone } from '@angular/core'
import { Observable, Observer } from 'rxjs'
import { filter } from 'rxjs/operators'

export function ifDefined<T>(
  source: Observable<T | null | undefined>,
): Observable<T> {
  return <any>source.pipe(filter(v => v !== null && v !== undefined))
}

export function onZone<T>(zone: NgZone) {
  // notice that we return a function here
  return function impl(source: Observable<T>): Observable<T> {
    return Observable.create((observer: Observer<T>) => {
      const onNext = (value: any) => zone.run(() => observer.next(value))
      const onError = (e: any) => zone.run(() => observer.error(e))
      const onComplete = () => zone.run(() => observer.complete())
      return source.subscribe(onNext, onError, onComplete)
    })
  }
}
