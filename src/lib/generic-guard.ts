import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  CanLoad,
  Route,
  RouterStateSnapshot,
} from '@angular/router'
import { Observable } from 'rxjs'

export abstract class GenericGuard
  implements CanActivate, CanActivateChild, CanLoad {
  canActivate(
    _next: ActivatedRouteSnapshot,
    _state: RouterStateSnapshot,
  ): Observable<boolean> {
    return this.validate()
  }

  canActivateChild(
    _next: ActivatedRouteSnapshot,
    _state: RouterStateSnapshot,
  ): Observable<boolean> {
    return this.validate()
  }

  canLoad(_route: Route): Observable<boolean> {
    return this.validate()
  }

  protected abstract validate(): Observable<boolean>
}
