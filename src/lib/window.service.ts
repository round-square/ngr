import { isPlatformBrowser } from '@angular/common'
import { Injectable, InjectionToken, PLATFORM_ID, inject } from '@angular/core'

/* Define abstract class for obtaining reference to the global window object. */
export abstract class WindowRef {
  get nativeWindow(): Window | Object {
    throw new Error('Not implemented.')
  }
}

/* Define class that implements the abstract class and returns the native window object. */
@Injectable({
  providedIn: 'root',
})
export class BrowserWindowRef extends WindowRef {
  get nativeWindow(): Window | Object {
    return window
  }
}

/* Create an factory function that returns the native window object. */
export function windowFactory(
  browserWindowRef: BrowserWindowRef,
  platformId: Object,
): Window | Object {
  if (isPlatformBrowser(platformId)) {
    return browserWindowRef.nativeWindow
  }
  return new Object()
}

/* Create a new injection token for injecting the window into a component. */
export const WindowToken = new InjectionToken('WindowToken', {
  providedIn: 'root',
  factory: () => windowFactory(inject(BrowserWindowRef), inject(PLATFORM_ID)),
})
