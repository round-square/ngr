import { Injectable, Type } from '@angular/core'
import { Actions } from '@ngrx/effects'
import { Store } from '@ngrx/store'
import { DataPersistence as NxDataPersistence } from '@nrwl/nx'
import {
  FetchOpts,
  HandleNavigationOpts,
  OptimisticUpdateOpts,
  PessimisticUpdateOpts,
} from '@nrwl/nx/src/data-persistence'
import { Observable } from 'rxjs'
import { Action, ActionClass } from './action'

export class UnknownError<T = any> extends Action {
  constructor(public error: T) {
    super()
  }
}

@Injectable({
  providedIn: 'root',
})
export class DataPersistence<S> {
  store: Store<S>
  actions: Actions

  constructor(private s: NxDataPersistence<S>) {
    this.store = s.store
    this.actions = s.actions
  }

  fetch<A extends Action>(
    action: ActionClass<A>,
    opts: FetchOpts<S, A>,
  ): Observable<any> {
    return this.s.fetch(action.prototype.type, {
      onError: (a, error) => this.onUnknownError(error),
      ...opts,
    })
  }

  optimisticUpdate<A extends Action>(
    action: ActionClass<A>,
    opts: OptimisticUpdateOpts<S, A>,
  ): Observable<any> {
    return this.s.optimisticUpdate(action.prototype.type, opts)
  }

  pessimisticUpdate<A extends Action>(
    action: ActionClass<A>,
    opts: PessimisticUpdateOpts<S, A>,
  ): Observable<any> {
    return this.s.pessimisticUpdate(action.prototype.type, opts)
  }

  navigation(
    component: Type<any>,
    opts: HandleNavigationOpts<S>,
  ): Observable<any> {
    return this.s.navigation(component, opts)
  }

  onUnknownError(error: any) {
    this.store.dispatch(new UnknownError(error))
  }
}
