import { NgZone } from '@angular/core'
import { first } from 'rxjs/operators'

export function executeOnStable(zone: NgZone, fn: () => any) {
  if (zone.isStable) {
    fn()
  } else {
    zone.onStable
      .asObservable()
      .pipe(first())
      .subscribe(fn)
  }
}
